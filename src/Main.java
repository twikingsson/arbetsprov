//Thom Wikingsson
public class Main {
	public static void main(String[] args) {
		if (args.length == 1) {
			System.out.println("Input: " + args[0]);
			String[] terms = getTerms(args[0]);
			for (String t : terms) System.out.println("Term: " + t);
			//Kolla s� det �r minst 2 termer
			if (terms.length >= 2) {
				RomanCalculator calculator = new RomanCalculator();
				try {
					System.out.println("Resultat: " + calculator.calc(terms));
				} catch (Exception e) {
					System.out.println("Exception: " + e.toString());
				}
			} else {
				System.out.println("Du m�ste ange minst 2 termer");
			}
		} else {
			System.out.println("Ange termerna som f�rsta argumentet (\"II + L + XI\")");
		}
	}
	
	//Konvertera alla termer till en string-array
	private static String[] getTerms(String input) {
		if (input.length() > 0) {
			String[] terms = input.split("\\+");
			for (int i = 0; i < terms.length; i++) {
				//Ta bort tomma tecken och konvertera till versaler
				terms[i] = terms[i].trim().toUpperCase();
			}
			return terms;
		} else {
			return null;
		}
	}
}
