import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RomanCalculator {
	private ArrayList<RomanNumber> romanNumbers = new ArrayList<RomanNumber>();
	
	public RomanCalculator() {
		addNumbers();
	}
	public ArrayList<RomanNumber> getRomanNumbers() {
		return this.romanNumbers;
	}
	public String calc(String[] terms) throws Exception {
		try {
			return calc(terms, false);
		} catch (Exception e) {
			throw e;
		}
	}
	public String calc(String[] terms, boolean debug) throws Exception {
		//Lista med alla nummer expanderade. S� man kan sortera och sl�ihop alla termer
		ArrayList<RomanNumber> expandList = new ArrayList<RomanNumber>(); 
		for (String term : terms) {
			//Expandera alla substraktiv (1)
			String expanded  = expandRoman(term);
			//Kolla s� att alla termen �r giltlig
			if (validTerm(expanded)) {
				try {
					//Kombinera (2)
					expandList.addAll(getExpandedRomanNumbers(expanded));
				} catch (Exception e) {
					throw e;
				}
			} else {
				throw new Exception("Termen " + term + " �r inte giltlig");
			}
		}
		//Sortera listan efter v�rde, h�gt till l�gt (3)
		Collections.sort(expandList);
		//Konvertera listan till en str�ng
		String result = expandList.stream().map(RomanNumber::getRoman).collect(Collectors.joining(""));
		if (debug) System.out.println("Expanded: " + result);
		//Kombinera grupper (4)
		result = combineGroups(result);
		if (debug) System.out.println("Groups: " + result);
		//Kombinera subtraktiv (5)
		result = combineSubstractives(result);
		if (debug) System.out.println("Subs: " + result);
		return result;
	}
	public void addNumbers() {
		//L�gger till siffror upp till 100
		romanNumbers.add(new RomanNumber("I", 1, "I"));
		romanNumbers.add(new RomanNumber("IV", 4, "IIII", true));
		romanNumbers.add(new RomanNumber("V", 5, "IIIII"));
		romanNumbers.add(new RomanNumber("IX", 9, "VIIII", true));
		romanNumbers.add(new RomanNumber("X", 10, "VV"));
		romanNumbers.add(new RomanNumber("XL", 40, "XXXX", true));
		romanNumbers.add(new RomanNumber("L", 50, "XXXXX"));
		romanNumbers.add(new RomanNumber("XC", 90, "LXXXX", true));
		romanNumbers.add(new RomanNumber("C", 100, "LL"));
	}
	public String combineGroups(String s) {
		String result = new String(s);
		//Filtrera ut alla grupper fr�n listan.
		List<RomanNumber> groupList = this.romanNumbers.stream().filter(n -> (n.isSubtract() == false && n.getValue() > 1)).collect(Collectors.toList());
		//V�nster -> H�ger (S� man b�rjar fr�n r�tt position n�r man ers�tter grupperna)
		for (int i = 0; i < result.length(); i++) {
			//B�rja med de h�gsta grupperna
			for (int j = groupList.size() - 1; j >= 0; j--) {
				RomanNumber n = groupList.get(j);
				int end = i + n.getExpanded().length();
				//Kolla s� att det expanderade numrets l�ngd inte �r out of range och att v�rdet �r lika
				if ((end <= result.length()) && (result.substring(i, end).equals(n.getExpanded()))) {
					//Byt ut gruppen
					result = result.substring(0, i) + n.getRoman() + result.substring(end);
					//B�rja om fr�n b�rjan
					i = -1;
					break;
				}
			}
		}
		return result;
	}
	public String combineSubstractives(String s) {
		String result = new String(s);
		//Filtrera ut substraktiv
		List<RomanNumber> subList = this.romanNumbers.stream().filter(n -> n.isSubtract() == true).collect(Collectors.toList());
		//H�ger -> V�nster
		for (int i = result.length() - 1; i >= 0; i--) {
			//B�rja med det h�gsta
			for (int j = subList.size() - 1; j >= 0; j--) {
				RomanNumber n = subList.get(j);
				int start = i - n.getExpanded().length() + 1;
				if (start >= 0 && n.getExpanded().equals(result.substring(start, start + n.getExpanded().length()))) {
					//Byt ut substrativet
					result = result.substring(0, start) + n.getRoman() + result.substring(i + 1);
					//B�rja innan nuvarande substraktiv
					i = start;
				}
			}
		}
		return result;
	}
	public String expandRoman(String r) {
		String result = new String(r);
		//Filtrera ut substraktiv
		List<RomanNumber> subList = this.romanNumbers.stream().filter(n -> n.isSubtract() == true).collect(Collectors.toList());
		for (int i = 0; i < result.length() - 1; i++) {
			//Substraktiv par
			String pair = result.substring(i, i + 2);
			for (RomanNumber sub : subList) {
				//Loppar igenom substraktiv och kolla om det finns n�got om byt ut substraktivet mot det expanderade v�rdet.
				if (pair.equals(sub.getRoman())) {
					result = result.substring(0, i) + sub.getExpanded() + result.substring(2 + i);
					i++;
					break;
				}
			}
		}
		return result;
	}
	public boolean validTerm(String term) {
		String expanded = term;
		//Kollar igenom en expanderad term s� att den �r korrekt. Kollar s� att h�gera v�rdet inte �r st�rre �n v�nstra.
		for (int i = 0; i < expanded.length() - 1; i++) {
			RomanNumber n1 = getRomanNumber(String.valueOf(expanded.charAt(i)));
			RomanNumber n2 = getRomanNumber(String.valueOf(expanded.charAt(i + 1)));
			if (n1 == null || n2 == null || n2.getValue() > n1.getValue()) return false;
		}
		return true;
	}
	public RomanNumber getRomanNumber(String s) {
		//Returner nummer objekt f�r en siffra
		List<RomanNumber> subList = this.romanNumbers.stream().filter(n -> n.isSubtract() == false).collect(Collectors.toList());
		for (RomanNumber number : subList) {
			if (number.getRoman().equals(s)) return number;
		}
		return null;
	}
	private ArrayList<RomanNumber> getExpandedRomanNumbers(String expanded) throws Exception {
		//Returnerar nummer f�r en expanderad term
		ArrayList<RomanNumber> list = new ArrayList<RomanNumber>();
		for (int i = 0; i < expanded.length(); i++) {
			String s = String.valueOf(expanded.charAt(i));
			RomanNumber number = getRomanNumber(s);
			if (number == null) throw new Exception("Kunde inte hitta numret " + s);
			list.add(number);
		}
		return list;
	}
}
