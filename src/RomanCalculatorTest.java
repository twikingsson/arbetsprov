import static org.junit.Assert.*;

import org.junit.Test;

public class RomanCalculatorTest {
	private RomanCalculator calculator = new RomanCalculator();
	
	@Test
	public void testGetRomanNumbers() {
		assertEquals(9, calculator.getRomanNumbers().size());
		assertFalse(calculator.getRomanNumbers().isEmpty());
	}

	@Test
	public void testCalcStringArray() {
		String[] terms = new String[] { "V", "V" };
		try {
			assertEquals("X", calculator.calc(terms));
		} catch (Exception e) {
			fail("Calculate exception: " + e.getMessage());
		}
	}

	@Test
	public void testCalcStringArrayBoolean() {
		String[] terms = new String[] { "V", "V" };
		try {
			assertEquals("X", calculator.calc(terms, false));
		} catch (Exception e) {
			fail("Calculate exception: " + e.getMessage());
		}
	}

	@Test
	public void testAddNumbers() {
		assertEquals(9, calculator.getRomanNumbers().size());
		assertFalse(calculator.getRomanNumbers().isEmpty());
		assertEquals(10, calculator.getRomanNumbers().get(4).getValue());
	}

	@Test
	public void testCombineGroups() {
		assertEquals("V", calculator.combineGroups("IIIII"));
		assertEquals("X", calculator.combineGroups("IIIIIIIIII"));
	}

	@Test
	public void testCombineSubstractives() {
		assertEquals("IV", calculator.combineSubstractives("IIII"));
		assertEquals("IX", calculator.combineSubstractives("VIIII"));
	}

	@Test
	public void testExpandRoman() {
		assertEquals("IIII", calculator.expandRoman("IV"));
		assertEquals("VIIII", calculator.expandRoman("IX"));
	}

	@Test
	public void testValidTerm() {
		assertTrue(calculator.validTerm("V"));
		assertFalse(calculator.validTerm("VX"));
	}
	
	@Test
	public void testGetRomanNumber() {
		assertNull(calculator.getRomanNumber("A"));
		assertNotNull(calculator.getRomanNumber("V"));
	}
}
