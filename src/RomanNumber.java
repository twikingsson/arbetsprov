
public class RomanNumber implements Comparable<RomanNumber> {
	private String roman;
	private int value;
	private String expanded;
	private boolean subtract;
	
	public RomanNumber(int value) throws Exception {
		if (value == 1) setValues("I", value, "I", false);
		else if (value == 4) setValues("IV", value, "IIII", true);
		else if (value == 5) setValues("V", value, "IIIII", false);
		else if (value == 9) setValues("IX", value, "VIIII", true);
		else if (value == 10) setValues("X", value, "VV", false);
		else if (value == 40) setValues("XL", value, "XXXX", true);
		else if (value == 50) setValues("L", value, "XXXXX", false);
		else if (value == 90) setValues("XC", value, "LXXXX", true);
		else if (value == 100) setValues("C", value, "LL", false);
		else throw new Exception("Not a valid number");
	}
	public RomanNumber(String roman, int value, String expanded) {
		setValues(roman, value, expanded, false);
	}
	public RomanNumber(String roman, int value, String expanded, boolean substract) {
		setValues(roman, value, expanded, substract);
	}
	
	private void setValues(String roman, int value, String expanded, boolean substract) {
		this.roman = roman;
		this.value = value;
		this.expanded = expanded;
		this.subtract = substract;
	}
	
	public String getRoman() {
		return roman;
	}
	public void setRoman(String roman) {
		this.roman = roman;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getExpanded() {
		return expanded;
	}
	public void setExpanded(String expanded) {
		this.expanded = expanded;
	}
	public boolean isSubtract() {
		return subtract;
	}
	public void setSubtract(boolean subtract) {
		this.subtract = subtract;
	}
	
	@Override
	public String toString() {
		return String.format("%s - %d - %s", this.roman, this.value, this.expanded);
	}
	
	 public int compareTo(RomanNumber rn) {
		 if (this.value == rn.getValue()) return 0;
		 else if (this.value > rn.getValue()) return -1;
		 else return 1;
	 }
}
