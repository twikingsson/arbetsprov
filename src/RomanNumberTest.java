import static org.junit.Assert.*;

import org.junit.Test;

public class RomanNumberTest {

	@Test
	public void testCompareTo() {
		try {
			RomanNumber r1 = new RomanNumber(1);
			RomanNumber r2 = new RomanNumber(5);
			assertEquals(1, r1.compareTo(r2));
		} catch (Exception e) {
			fail("No valid numbers");
		}
	}
}
